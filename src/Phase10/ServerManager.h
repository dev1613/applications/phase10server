/**
 * @file ServerManager.h
 *
 * @brief Manager class for Phase 10 server
 *
 * @ingroup Server
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <atomic>
#include <list>
#include <thread>

#include <Common/ThreadSafeData.h>
#include <Common/ThreadSafeQueue.h>
#include <Network/Common.h>
#include <Phase10/CommonTypes.h>
#include <System/Logger.h>
#include <System/SerialBuffer.h>
#include <System/Serializable.h>

namespace Phase10
{
  // Forward declarations
  class UserConnections;

  //-------------------------------------------------------------------------//
  // Class:       ClientMsg
  // Description: Struct for message received by client
  //-------------------------------------------------------------------------//
  struct ClientMsg
  {
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param _pSocket - client socket message was received from
    /// @param _pMsg - message data
    ///////////////////////////////////////////////////////////////////////////
    ClientMsg(Network::Socket* _pSocket = nullptr, System::sSerialBuffer _pMsg = nullptr) :
      pSocket(_pSocket), pMsg(_pMsg) {}

    // Struct variables
    Network::Socket*      pSocket; ///< client socket message was received from
    System::sSerialBuffer pMsg;    ///< message data
  };

  //-------------------------------------------------------------------------//
  // Class:       ServerMsg
  // Description: Struct for message sent out to client
  //-------------------------------------------------------------------------//
  struct ServerMsg
  {
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param _id - message ID
    /// @param _pSocket - client socket to send message on
    /// @param _pMsg - message to send
    ///////////////////////////////////////////////////////////////////////////
    ServerMsg(uint32_t _id = 0, Network::Socket* _pSocket = nullptr, System::sSerializable _pMsg = nullptr) :
      id(_id), pSocket(_pSocket), pMsg(_pMsg) {}

    // Struct variables
    uint32_t              id;      ///< message ID
    Network::Socket*      pSocket; ///< client socket to send message on
    System::sSerializable pMsg;    ///< message data
  };

  //-------------------------------------------------------------------------//
  // Class:       ServerManager
  // Description: Manager class for Phase 10 server
  //-------------------------------------------------------------------------//
  class ServerManager
  {
  public:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param pLogger - debug logger
    /// @param pGameFactory - game factory
    /// @param pUserFactory - user factory
    ///////////////////////////////////////////////////////////////////////////
    ServerManager(System::sLogger pLogger, sGameFactory pGameFactory, sUserFactory pUserFactory);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~ServerManager();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Starts the server
    /// @param port - port number to receive open connections
    /// @return - true if started
    ///////////////////////////////////////////////////////////////////////////
    bool start(uint32_t port);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Stops the server
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void stop();

  private:
    // Private types
    using ClientList = std::list<Network::sSocket>;

    // Private class variables
    const System::sLogger mLogger; ///< debug logger

    const sGameFactory mGameFactory; ///< game factory
    const sUserFactory mUserFactory; ///< user factory

    Network::sSocket mServerSocket; ///< server socket

    Common::ThreadSafeData<ClientList> mClients;   ///< open clients
    Common::ThreadSafeQueue<ClientMsg> mMsgQueue;  ///< message queue
    Common::ThreadSafeQueue<ServerMsg> mSendQueue; ///< send message queue

    std::unique_ptr<UserConnections> mUsers; ///< user connections map

    std::atomic<bool> mRunning;       ///< server running?
    std::thread       mControlThread; ///< control thread
    std::thread       mSendThread;    ///< sending thread

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when a new client connects to the server
    /// @param pClient - client socket to add
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void clientAdded(Network::sSocket pClient);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Thread to handle control processing
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void controlThread();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when a message is received from a socket
    /// @param pClient - client socket message was received from
    /// @param pMsg - message data
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void msgReceived(Network::Socket* pClient, System::sSerialBuffer pMsg);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Sends open games to client(s)
    /// @param pClient - client to send to, or nullptr to send to everyone
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void sendOpenGames(Network::Socket* pClient = nullptr);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Sends game members update to all members of the game
    /// @param pGame - game to update
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void sendGameMembers(const Game* pGame);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Sends game state update to all members of the game
    /// @param pGame - game to update
    /// @param pSource - user that originated the update
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void sendGameState(const Game* pGame, const User* pSource = nullptr);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Thread to handle sending out messages
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void sendThread();
  };

} // namespace Phase10
