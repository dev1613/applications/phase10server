/**
 * @file ServerManager.cpp
 *
 * @brief Manager class for Phase 10 server
 *
 * @ingroup Server
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <Network/Socket.h>
#include <Phase10/Game.h>
#include <Phase10/Messages.h>
#include <Phase10/Player.h>
#include <Phase10/ServerManager.h>
#include <Phase10/User.h>
#include <Phase10/UserConnections.h>

namespace Phase10
{
  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  ServerManager::ServerManager(System::sLogger pLogger, sGameFactory pGameFactory, sUserFactory pUserFactory) :
    mLogger(pLogger), mGameFactory(pGameFactory), mUserFactory(pUserFactory),
    mRunning(false)
  {
    mUsers.reset(new UserConnections());
  }

  /////////////////////////////////////////////////////////////////////////////
  // Destructor
  /////////////////////////////////////////////////////////////////////////////
  ServerManager::~ServerManager()
  {
    stop();
  }

  /////////////////////////////////////////////////////////////////////////////
  // start
  /////////////////////////////////////////////////////////////////////////////
  bool ServerManager::start(uint32_t port)
  {
    // Start server socket
    bool success = false;
    if(mServerSocket)
    {
      mLogger->debug("Server already started");
    }
    else
    {
      using namespace std::placeholders;
      auto acceptCallback = std::bind(&ServerManager::clientAdded, this, _1);
      auto dataCallback = std::bind(&ServerManager::msgReceived, this, _1, _2);
      mServerSocket = std::make_shared<Network::Socket>(mLogger, acceptCallback, dataCallback, port);
    }

    // Start threads
    success = mServerSocket->valid();
    if(success && !mRunning.exchange(true))
    {
      mControlThread = std::thread(&ServerManager::controlThread, this);
      mSendThread = std::thread(&ServerManager::sendThread, this);
    }

    return true;
  }

  /////////////////////////////////////////////////////////////////////////////
  // stop
  /////////////////////////////////////////////////////////////////////////////
  void ServerManager::stop()
  {
    if(mRunning.exchange(false))
    {
      // Stop control thread
      mLogger->debug("ServerManager::%s - Stopping control thread", __func__);
      mMsgQueue.notify();
      if(mControlThread.joinable())
      {
        mControlThread.join();
      }
    
      // Stop sending thread
      mLogger->debug("ServerManager::%s - Stopping sending thread", __func__);
      mSendQueue.notify();
      if(mSendThread.joinable())
      {
        mSendThread.join();
      }

      // Close server socket so we stop accepting connections
      mLogger->debug("ServerManager::%s - Closing server socket", __func__);
      mServerSocket.reset();

      // Close all client sockets
      mLogger->debug("ServerManager::%s - Closing client sockets", __func__);
      mClients.set(ClientList());
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // clientAdded
  /////////////////////////////////////////////////////////////////////////////
  void ServerManager::clientAdded(Network::sSocket pClient)
  {
    mLogger->debug("SocketManager::%s - New client connected", __func__);
    mClients.set([pClient](ClientList& clients)
    {
      clients.push_back(pClient);
    });
  }

  /////////////////////////////////////////////////////////////////////////////
  // controlThread
  /////////////////////////////////////////////////////////////////////////////
  void ServerManager::controlThread()
  {
    mLogger->debug("ServerManager::%s - Entering", __func__);

    // Local variables
    ClientMsg clientMsg;

    // Function to check for end of server
    auto exitCriteria = [this]()
    {
      return !mRunning.load();
    };

    // Function to check if socket matches the client
    auto isClientSocket = [&clientMsg](const Network::sSocket& pSocket)
    {
      return(pSocket.get() == clientMsg.pSocket);
    };

    // Function to get the iterator for the client socket
    auto getClientSocket = [&clientMsg, &isClientSocket](ClientList& clients)
    {
      return std::find_if(clients.begin(), clients.end(), isClientSocket);
    };

    // Function to check if a socket is valid
    bool validSocket = false;
    auto checkSocketValid = [&clientMsg, &getClientSocket, &validSocket](ClientList& clients)
    {
      auto it = getClientSocket(clients);
      validSocket = (it != clients.end());
    };

    while(mRunning.load())
    {
      // Wait for next message
      clientMsg.pSocket = nullptr;
      clientMsg.pMsg    = nullptr;

      if(!mMsgQueue.remove(clientMsg, true, exitCriteria) ||
          clientMsg.pSocket == nullptr || !mRunning.load())
      {
        mLogger->debug("ServerManager::%s - No message received", __func__);
        continue;
      }

      // Make sure it's a valid socket
      validSocket = false;
      mClients.get(checkSocketValid, false);
      if(!validSocket)
      {
        mLogger->warn("ServerManager::%s - Received message from unrecognized socket", __func__);
        continue;
      }

      // Get user
      auto pUser = mUsers->getUser(clientMsg.pSocket);

      // Check if socket was disconnected
      if(clientMsg.pMsg == nullptr || !clientMsg.pSocket->valid())
      {
        static const std::string UNNAMED("N/A");
        auto& name = pUser ? pUser->name() : UNNAMED;
        uint32_t id = pUser ? pUser->id() : 0;

        mLogger->info("ServerManager::%s - Lost connection with %s [ID %u]",
                      __func__, name.c_str(), id);

        mUsers->remove(pUser);
        mClients.set([&getClientSocket](ClientList& clients)
        {
          auto it = getClientSocket(clients);
          if(it != clients.end())
          {
            clients.erase(it);
          }
        });


        // Remove user from game
        auto* pGame = pUser ? pUser->game() : nullptr;
        if(pGame)
        {
          pUser->leave();
          sendOpenGames();
          sendGameMembers(pGame);
          sendGameState(pGame);
        }

        continue;
      }

      // Process message
      uint32_t msgID = clientMsg.pMsg->msgID();
      switch(msgID)
      {
        // Request new user be created
        case MSG_REQUEST_USER:
        {
          Messages::RequestUserMsg requestMsg;
          if(pUser)
          {
            mLogger->warn("ServerManager::%s - Got new user request for existing user %s [%u]",
                          __func__, pUser->name().c_str(), pUser->id());
          }
          else if(!requestMsg.decode(*clientMsg.pMsg))
          {
            mLogger->error("ServerManager::%s - Failed to decode user request message", __func__);
          }
          else
          {
            pUser = mUserFactory->create(requestMsg.name).get();
            if(pUser)
            {
              mLogger->debug("ServerManager::%s - Created user %s [%u]",
                              __func__, pUser->name().c_str(), pUser->id());

              // Add to connections
              if(!mUsers->add(pUser, clientMsg.pSocket))
              {
                mLogger->warn("ServerManager::%s - Failed to add %s [%u] to connection map",
                              __func__, pUser->name().c_str(), pUser->id());
              }

              // Send response
              auto pResponseMsg  = std::make_shared<Messages::UserMsg>();
              pResponseMsg->id   = pUser->id();
              pResponseMsg->name = pUser->name();

              ServerMsg sendMsg(MSG_NEW_USER, clientMsg.pSocket, std::move(pResponseMsg));
              mSendQueue.add(std::move(sendMsg));

              // Broadcast open games to this user only
              sendOpenGames(clientMsg.pSocket);
            }
            else
            {
              mLogger->error("ServerManager::%s - Failed to create new user %s",
                              __func__, requestMsg.name);
            }
          }
          break;
        }

        // Request game creation
        case MSG_REQUEST_GAME_CREATE:
        {
          Messages::RequestGameMsg requestMsg;
          if(pUser == nullptr)
          {
            mLogger->warn("ServerManager::%s - Unregistered user tried to create game", __func__);
          }
          else if(!requestMsg.decode(*clientMsg.pMsg))
          {
            mLogger->error("ServerManager::%s - Failed to decode game creation request message", __func__);
          }
          else
          {
            auto pGame = mGameFactory->create(requestMsg.name);
            if(pGame && pUser->join(pGame.get()))
            {
              mLogger->debug("ServerManager::%s - Created game %s [%u] with player %s [%u]",
                              __func__, pGame->name().c_str(), pGame->id(),
                              pUser->name().c_str(), pUser->id());

              // Send game members and open games
              sendOpenGames();
              sendGameMembers(pGame.get());
            }
            else
            {
              //TODO: Remove game from factory
              mLogger->warn("ServerManager::%s - Failed to create game for %s [%u]",
                            __func__, pUser->name().c_str(), pUser->id());
            }
          }
          break;
        }

        // Request to join existing game
        case MSG_REQUEST_GAME_JOIN:
        {
          Messages::RequestJoinGameMsg requestMsg;
          if(pUser == nullptr)
          {
            mLogger->warn("ServerManager::%s - Unregistered user tried to join game", __func__);
          }
          else if(!requestMsg.decode(*clientMsg.pMsg))
          {
            mLogger->error("ServerManager::%s - Failed to decode join game request message", __func__);
          }
          else
          {
            auto pGame = mGameFactory->find(requestMsg.gameID);
            if(pGame && pUser->join(pGame.get()))
            {
              mLogger->debug("ServerManager::%s - Added %s [%u] to %s [%u]",
                              __func__, pUser->name().c_str(), pUser->id(),
                              pGame->name().c_str(), pGame->id());

              // Send game members and open games
              sendOpenGames();
              sendGameMembers(pGame.get());
            }
            else
            {
              mLogger->warn("ServerManager::%s - Failed to add %s [%u] to game %u",
                            __func__, pUser->name().c_str(), pUser->id(),
                            requestMsg.gameID);
            }
          }
          break;
        }

        // Request next round of game be dealt
        case MSG_REQUEST_DEAL:
        {
          if(pUser == nullptr)
          {
            mLogger->warn("ServerManager::%s - Unregistered user tried to deal game", __func__);
          }
          else if(pUser->game() == nullptr)
          {
            mLogger->warn("ServerManager::%s - %s [%u] tried to start game before joining one",
                          __func__, pUser->name().c_str(), pUser->id());
          }
          else
          {
            auto* pGame = pUser->game();
            if(pGame->startRound())
            {
              mLogger->debug("ServerManager::%s - Started next round of %s [%u]",
                              __func__, pGame->name().c_str(), pGame->id());

              // Send game state and open games
              sendOpenGames();
              sendGameState(pGame);
            }
            else
            {
              mLogger->warn("ServerManager::%s - Failed to start next round of %s [%u]",
                            __func__, pGame->name().c_str(), pGame->id());
            }
          }
          break;
        }

        // Game state update from player
        case MSG_GAME_UPDATE:
        {
          Messages::GameStateMsg stateMsg;
          if(pUser == nullptr)
          {
            mLogger->warn("ServerManager::%s - Unregistered user tried to update game state", __func__);
          }
          else if(!stateMsg.decode(*clientMsg.pMsg))
          {
            mLogger->warn("ServerManager::%s - Failed to decode game state update", __func__);
          }
          else
          {
            auto* pGame = pUser->game();
            if(pGame && pGame->id() == stateMsg.gameID)
            {
              mLogger->debug("ServerManager::%s - Processing game state update from %s [%u]",
                              __func__, pUser->name().c_str(), pUser->id());

              bool playerAdvanced = false;
              pGame->setState(stateMsg, playerAdvanced);

              // Send game state and open games
              sendOpenGames();

              // Only send state back to source player if player was advanced
              if(playerAdvanced)
              {
                sendGameState(pGame);
              }
              else
              {
                sendGameState(pGame, pUser);
              }
            }
            else
            {
              mLogger->warn("ServerManager::%s - %s [%u] tried to update state for another game",
                            __func__, pUser->name().c_str(), pUser->id());
            }
          }
          break;
        }

        // Unhandled message ID
        default:
        {
          mLogger->error("ServerManager::%s - Received message with unrecognized ID %u",
                          __func__, msgID);
          break;
        }
      }
    }

    mLogger->debug("ServerManager::%s - Exiting", __func__);
  }

  /////////////////////////////////////////////////////////////////////////////
  // msgReceived
  /////////////////////////////////////////////////////////////////////////////
  void ServerManager::msgReceived(Network::Socket* pClient, System::sSerialBuffer pMsg)
  {
    // Add to message queue
    ClientMsg clientMsg(pClient, std::move(pMsg));
    mMsgQueue.add(std::move(clientMsg));
  }

  /////////////////////////////////////////////////////////////////////////////
  // sendGameMembers
  /////////////////////////////////////////////////////////////////////////////
  void ServerManager::sendGameMembers(const Game* pGame)
  {
    // Create message
    auto pMsg = std::make_shared<Messages::GameMembersMsg>();
    pGame->getMembers(*pMsg);

    // Send to players
    mLogger->debug("ServerManager::%s - %lu members in game %s [%u]",
      __func__, pMsg->players.size(), pGame->name().c_str(), pGame->id());

    ServerMsg msg(MSG_GAME_MEMBERS, nullptr, pMsg);
    for(auto& player : pMsg->players)
    {
      auto* pSocket = mUsers->getSocket(player.id);
      if(pSocket && pSocket->valid())
      {
        msg.pSocket = pSocket;
        mSendQueue.add(msg);
      }
      else
      {
        mLogger->error("ServerManager::%s - Lost connection with %s [%u]",
          __func__, player.name.c_str(), player.id);
      }
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // sendGameState
  /////////////////////////////////////////////////////////////////////////////
  void ServerManager::sendGameState(const Game* pGame, const User* pSource)
  {
    // Create message
    auto pMsg = std::make_shared<Messages::GameStateMsg>();
    pGame->getState(*pMsg);

    // Don't send back to source
    uint32_t sourceID = (pSource ? pSource->id() : UserFactory::INVALID_ID);

    // Send to players
    ServerMsg msg(MSG_GAME_UPDATE, nullptr, std::move(pMsg));
    for(auto& pPlayer : pGame->getPlayers())
    {
      if(pPlayer->id() != sourceID)
      {
        auto* pSocket = mUsers->getSocket(pPlayer->id());
        if(pSocket && pSocket->valid())
        {
          mLogger->debug("ServerManager::%s - Send %s [%u] state to %s [%u]",
            __func__, pGame->name().c_str(), pGame->id(),
            pPlayer->name().c_str(), pPlayer->id());

          msg.pSocket = pSocket;
          mSendQueue.add(msg);
        }
      }
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // sendOpenGames
  /////////////////////////////////////////////////////////////////////////////
  void ServerManager::sendOpenGames(Network::Socket* pClient)
  {
    // Create message
    auto pMsg = std::make_shared<Messages::OpenGamesMsg>();
    auto allGames = mGameFactory->allInstances();
    for(auto& pGame : allGames)
    {
      if(pGame->numPlayers() > 0)
      {
        pGame->getInfo(pMsg->games.emplace_back());
      }
    }

    // Send to client(s)
    mLogger->debug("ServerManager::%s - %lu open games", __func__, pMsg->games.size());
    ServerMsg msg(MSG_OPEN_GAMES, pClient, std::move(pMsg));
    if(pClient && pClient->valid())
    {
      mSendQueue.add(std::move(msg));
    }
    else
    {
      // Get all clients
      ClientList clients;
      mClients.get(clients, false);
      for(auto& pSocket : clients)
      {
        if(pSocket->valid())
        {
          msg.pSocket = pSocket.get();
          mSendQueue.add(msg);
        }
      }
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // sendThread
  /////////////////////////////////////////////////////////////////////////////
  void ServerManager::sendThread()
  {
    mLogger->debug("ServerManager::%s - Entering", __func__);

    // Local variables
    ServerMsg            sendMsg;
    System::SerialBuffer msgBuffer;

    // Function to check for end of server
    auto exitCriteria = [this]()
    {
      return !mRunning.load();
    };

    while(mRunning.load())
    {
      // Get next message
      if(!mSendQueue.remove(sendMsg, true, exitCriteria) || !mRunning.load())
      {
        continue;
      }

      // Check for socket open
      if(!sendMsg.pSocket->valid())
      {
        mLogger->warn("ServerManager::%s - Cannot send message on closed socket", __func__);
        continue;
      }

      // Encode message
      msgBuffer.clear();
      msgBuffer.setMsgID(sendMsg.id);
      
      if(sendMsg.pMsg)
      {
        sendMsg.pMsg->encode(msgBuffer);
      }

      // Send message
      if(!sendMsg.pSocket->sendMsg(msgBuffer))
      {
        mLogger->error("ServerManager::%s - Failed to send message with ID %u", __func__, sendMsg.id);
      }
    }

    mLogger->debug("ServerManager::%s - Exiting", __func__);
  }

} // namespace Phase10
