/**
 * @file main.cpp
 *
 * @brief Main entry point for server application
 *
 * @ingroup Server
 *
 * @author Devon Johnson
 *
 */
 
 // Includes
#include <iostream>
#include <thread>

#include <Network/Socket.h>
#include <System/Logger.h>
#include <System/SerialBuffer.h>

// Global variables
static System::sLogger pLogger;
static Network::sSocket pSocket;

// Callback for client connected
static void clientConnected(Network::sSocket pClient)
{
  pLogger->debug("Accepted new client connection");
  pSocket = pClient;
}

// Callback for message received
static void msgReceived(Network::Socket* pClient, System::sSerialBuffer pBuffer)
{
  pLogger->debug("Message ID: %u", pBuffer->msgID());

  std::string msg;
  if(pBuffer->read(msg))
  {
    pLogger->debug("Message: %s", msg.c_str());
  }
  else
  {
    pLogger->error("Failed to read message");
  }

  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  pBuffer->setMsgID(pBuffer->msgID() + 1);
  if(pSocket->sendMsg(*pBuffer))
  {
    pLogger->debug("Sent message back");
  }
  else
  {
    pLogger->error("Failed to send");
  }
}

// Main function
int main(int argc, char* argv[])
{
  // Check args
  if(argc < 2)
  {
    std::cerr << "Usage: " << argv[0] << " port [server address]" << std::endl;
    return -1;
  }

  // Get port and hostname
  uint32_t port = std::atoi(argv[1]);
  bool server = false;
  std::string hostname;
  if(argc > 2)
  {
    hostname = argv[2];
  }
  else
  {
    hostname = "NULL";
    server = true;
  }

  // Create logger
  pLogger = std::make_shared<System::Logger>();
  pLogger->setLevel(System::LOG_DEBUG);

  // Create socket
  Network::sSocket pServerSocket;
  if(server)
  {
    pServerSocket = std::make_shared<Network::Socket>(pLogger, clientConnected, msgReceived, port);
    pSocket = pServerSocket;
  }
  else
  {
    pSocket = std::make_shared<Network::Socket>(pLogger, msgReceived, port, hostname);
  }

  if(!pSocket->valid())
  {
    pLogger->fatal("Failed to create socket for %s:%u", hostname.c_str(), port);
    return -1;
  }

  // Initialize ping pong
  if(pSocket)
  {
    if(!server)
    {
      // Send initial message
      std::string hello("hello there");
      System::SerialBuffer buffer;
      buffer.setMsgID(1);
      buffer.append(hello);
      if(pSocket->sendMsg(buffer))
      {
        pLogger->debug("Sent initial startup message");
      }
      else
      {
        pLogger->fatal("Failed to sent initial startup message");
        return -1;
      }
    }
  }
  else
  {
    pLogger->fatal("Failed to initialize");
    return -1;
  }

  // Wait for terminate
  std::string input;
  while(pSocket->valid())
  {
    std::cin >> input;
    if(input == "q")
    {
      pSocket->closeConnection();
    }
  }

  return 0;
} // End - main
