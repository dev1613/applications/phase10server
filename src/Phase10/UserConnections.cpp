/**
 * @file UserConnections.cpp
 *
 * @brief Maps the users to/from their respective connections
 *
 * @ingroup Server
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <Network/Socket.h>
#include <Phase10/User.h>
#include <Phase10/UserConnections.h>

namespace Phase10
{
  /////////////////////////////////////////////////////////////////////////////
  // add
  /////////////////////////////////////////////////////////////////////////////
  bool UserConnections::add(User* pUser, Network::Socket* pSocket)
  {
    bool success = false;
    if(mSocketToUser.find(pSocket) == mSocketToUser.end() &&
       mUserToSocket.find(pUser) == mUserToSocket.end())
    {
      success = true;
      mSocketToUser[pSocket] = pUser;
      mUserToSocket[pUser] = pSocket;
    }

    return success;
  }

  /////////////////////////////////////////////////////////////////////////////
  // getSocket
  /////////////////////////////////////////////////////////////////////////////
  Network::Socket* UserConnections::getSocket(User* pUser) const
  {
    auto it = mUserToSocket.find(pUser);
    if(it != mUserToSocket.end())
    {
      return it->second;
    }
    else
    {
      return nullptr;
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // getSocket
  /////////////////////////////////////////////////////////////////////////////
  Network::Socket* UserConnections::getSocket(uint32_t userID) const
  {
    auto it = std::find_if(mUserToSocket.begin(), mUserToSocket.end(),
      [userID](const std::pair<User* const, Network::Socket*>& entry)
    {
      return(entry.first->id() == userID);
    });

    if(it != mUserToSocket.end())
    {
      return it->second;
    }
    else
    {
      return nullptr;
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // getUser
  /////////////////////////////////////////////////////////////////////////////
  User* UserConnections::getUser(Network::Socket* pSocket) const
  {
    auto it = mSocketToUser.find(pSocket);
    if(it != mSocketToUser.end())
    {
      return it->second;
    }
    else
    {
      return nullptr;
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // remove
  /////////////////////////////////////////////////////////////////////////////
  void UserConnections::remove(User* pUser)
  {
    auto it = mUserToSocket.find(pUser);
    if(it != mUserToSocket.end())
    {
      auto* pSocket = it->second;
      mUserToSocket.erase(it);

      auto it2 = mSocketToUser.find(pSocket);
      if(it2 != mSocketToUser.end())
      {
        mSocketToUser.erase(it2);
      }
    }
  }

} // namespace Phase10
