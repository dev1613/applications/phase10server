/**
 * @file main.cpp
 *
 * @brief Main entry point for Server
 *
 * @ingroup Server
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <Phase10/Game.h>
#include <Phase10/ServerConfiguration.h>
#include <Phase10/ServerManager.h>
#include <Phase10/User.h>
#include <System/Logger.h>

// Constants
static const std::string DEFAULT_CONFIG_FILE("../config/ServerConfig.xml");

///////////////////////////////////////////////////////////////////////////////
// main
///////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[])
{
  // Create logger
  auto pLogger = std::make_shared<System::Logger>();

  // Get config file
  std::string configFile;
  if(argc > 1)
  {
    configFile = argv[1];
  }
  else
  {
    configFile = DEFAULT_CONFIG_FILE;
  }

  // Parse configuration
  Phase10::ServerConfigurationParser parser(pLogger);
  auto& config = parser.config();
  if(parser.parse(configFile))
  {
    pLogger->setLevel(config.logLevel);
    pLogger->debug("Parsed server configuration from %s", configFile.c_str());
  }
  else
  {
    pLogger->fatal("Failed to parse server configuration from %s", configFile.c_str());
    return -1;
  }

  // Create factories
  auto pGameFactory = std::make_shared<Phase10::GameFactory>(pLogger);
  auto pUserFactory = std::make_shared<Phase10::UserFactory>(pLogger);

  if(pGameFactory && pUserFactory)
  {
    pLogger->debug("Created factories");
  }
  else
  {
    pLogger->fatal("Failed to create factories");
    return -1;
  }

  // Create manager
  auto pManager = std::make_shared<Phase10::ServerManager>(pLogger, pGameFactory, pUserFactory);
  if(pManager)
  {
    pLogger->debug("Created Server manager");
  }
  else
  {
    pLogger->fatal("Failed to create Server manager");
    return -1;
  }

  // Start manager
  if(pManager->start(config.port))
  {
    pLogger->info("Started server on port %u", config.port);
  }
  else
  {
    pLogger->fatal("Failed to start server on port %u", config.port);
    return -1;
  }

  // Wait for exit signal
  std::string input;
  while(input != "q")
  {
    std::cin >> input;
  }

  // Stop server
  pLogger->info("Stopping server");
  pManager->stop();

  // Destroy
  pManager.reset();
  pLogger.reset();

  return 0;
}
