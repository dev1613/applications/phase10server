/**
 * @file ServerConfiguration.h
 *
 * @brief Class to hold Phase 10 server configuration
 *
 * @ingroup Server
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <System/Logger.h>

namespace rapidxml
{
  template <class Ch = char>
  class xml_node;
}

namespace Phase10
{
  //-------------------------------------------------------------------------//
  // Class:       ServerConfiguration
  // Description: Class to hold Phase 10 server configuration
  //-------------------------------------------------------------------------//
  struct ServerConfiguration
  {
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    ///////////////////////////////////////////////////////////////////////////
    ServerConfiguration();

    // Struct variables
    uint32_t         port;     ///< server port
    System::LogLevel logLevel; ///< logging level
  };

  //-------------------------------------------------------------------------//
  // Class:       ServerConfigurationParser
  // Description: Class to parse Phase 10 server configuration
  //-------------------------------------------------------------------------//
  class ServerConfigurationParser
  {
  public:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param pLogger - debug logger
    ///////////////////////////////////////////////////////////////////////////
    ServerConfigurationParser(System::sLogger pLogger);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~ServerConfigurationParser() = default;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the configuration
    /// @return - configuration data
    ///////////////////////////////////////////////////////////////////////////
    const ServerConfiguration& config() const { return mConfig; }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Parses the configuration
    /// @param configFile - XML config file
    /// @return - true if parsed, else false
    ///////////////////////////////////////////////////////////////////////////
    bool parse(const std::string& configFile);

  private:
    // Private class variables
    const System::sLogger mLogger; ///< debug logger

    ServerConfiguration mConfig; ///< config data

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Parses the configuration tree
    /// @param root - root node
    /// @return - true if parsed, else false
    ///////////////////////////////////////////////////////////////////////////
    bool parseTree(const rapidxml::xml_node<>& root);
  };

} // namespace Phase10
