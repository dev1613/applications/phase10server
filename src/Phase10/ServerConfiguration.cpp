/**
 * @file ServerConfiguration.h
 *
 * @brief Class to hold Phase 10 server configuration
 *
 * @ingroup Server
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <rapidxml_utils.hpp>

#include <Phase10/ServerConfiguration.h>

// Constants
static const std::string KEY_LOG_LEVEL("LogLevel");
static const std::string KEY_PORT("Port");

namespace Phase10
{
  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  ServerConfiguration::ServerConfiguration() :
    port(0), logLevel(System::LOG_INFO)
  {
  }

  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  ServerConfigurationParser::ServerConfigurationParser(System::sLogger pLogger) :
    mLogger(pLogger)
  {
  }

  /////////////////////////////////////////////////////////////////////////////
  // parse
  /////////////////////////////////////////////////////////////////////////////
  bool ServerConfigurationParser::parse(const std::string& configFile)
  {
    bool success = false;

    mLogger->info("ServerConfigurationParser::%s - Parsing configuration at %s",
                  __func__, configFile.c_str());

    try
    {
      // Parse file
      rapidxml::file<> xmlFile(configFile.c_str());
      auto pDoc = std::make_shared<rapidxml::xml_document<>>();
      pDoc->parse<0>(xmlFile.data());

      // Parse tree
      auto pRootNode = pDoc->first_node();
      if(pRootNode)
      {
        success = parseTree(*pRootNode);
      }
    }
    catch(std::exception& ex)
    {
      mLogger->error("Error while parsing %s: %s", configFile.c_str(), ex.what());
    }

    return success;
  }

  /////////////////////////////////////////////////////////////////////////////
  // parseTree
  /////////////////////////////////////////////////////////////////////////////
  bool ServerConfigurationParser::parseTree(const rapidxml::xml_node<>& root)
  {
    // Parse tree
    bool foundPort = false;
    for(auto* pChild = root.first_node(); pChild; pChild = pChild->next_sibling())
    {
      if(pChild->type() == rapidxml::node_element)
      {
        if(pChild->name() == KEY_LOG_LEVEL)
        {
          System::GetLogLevel(mConfig.logLevel, pChild->value());
        }
        else if(pChild->name() == KEY_PORT)
        {
          mConfig.port = std::stoul(pChild->value());
          foundPort = true;
        }
        else
        {
          mLogger->warn("ServerConfigurationParser::%s - Unrecognized tag %s in tree",
                        __func__, pChild->name());
        }
      }
    }

    // Must have port
    if(!foundPort)
    {
      mLogger->warn("ServerConfigurationParser::%s - Missing %s in config file",
                    __func__, KEY_PORT.c_str());
    }

    return foundPort;
  }

} // namespace Phase10
