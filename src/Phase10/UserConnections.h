/**
 * @file UserConnections.h
 *
 * @brief Maps the users to/from their respective connections
 *
 * @ingroup Server
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <map>

#include <Network/Common.h>
#include <Phase10/CommonTypes.h>

namespace Phase10
{
  //-------------------------------------------------------------------------//
  // Class:       UserConnections
  // Description: Maps the users to/from their respective connections
  //-------------------------------------------------------------------------//
  class UserConnections
  {
  public:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    ///////////////////////////////////////////////////////////////////////////
    UserConnections() = default;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~UserConnections() = default;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Adds a user to the map
    /// @param pUser - user to add
    /// @param pSocket - socket connected with the user
    /// @return - true if added, else false
    ///////////////////////////////////////////////////////////////////////////
    bool add(User* pUser, Network::Socket* pSocket);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the socket that corresponds with the user
    /// @param pUser - user to find
    /// @return - socket found, or nullptr if it doesn't exist
    ///////////////////////////////////////////////////////////////////////////
    Network::Socket* getSocket(User* pUser) const;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the socket that corresponds with the user ID
    /// @param userID - user ID to find
    /// @return - socket found, or nullptr if it doesn't exist
    ///////////////////////////////////////////////////////////////////////////
    Network::Socket* getSocket(uint32_t userID) const;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the user that corresponds with the socket
    /// @param pSocket - socket to find
    /// @return - user found, or nullptr if it doesn't exist
    ///////////////////////////////////////////////////////////////////////////
    User* getUser(Network::Socket* pSocket) const;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Removes a user
    /// @param pUser - user to remove
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void remove(User* pUser);

  private:
    // Private class variables
    std::map<Network::Socket*, User*> mSocketToUser; ///< connection -> user
    std::map<User*, Network::Socket*> mUserToSocket; ///< user -> connection
  };

} // namespace Phase10
